# Workflows - The Basics

## Table of Contents

* Get the starter kits
* Configure build tools & project settings
* Download frameworks & libraries
* Setup a repo on Bitbucket
* Setup a staging server on CloudWays
* Setup deployment targets
* Save server & access credentials

---

## Get project template

Our projects comes in different sizes & shapes, but for some of the more common ones, we have created _starter kits_ to help you get started quickly.

* **Static websites**. Available in: [Vanilla](#) or [Foundation](#)
* **Custom WordPress themes**. Available in: [Vanilla](#) or Foundation (coming soon)
* **Custom Laravel applications** (coming soon)

**Vanilla** means plain, no added libraries whatsoever. **Foundation** refers to the UI library made by [ZURB](#).

Once downloaded, extract and move the folder to your working directory, and rename it accordingly. Then open your terminal to go into the folder:

    cd ~/htdocs/project-name

And run this command:

    npm install && bower install

It will download all the core dependencies that we will need to configure in a few moments.

---

## Configure build tools & project settings

[Grunt](#) is included in the starter kits to automate repetitive tasks. Here's some of the available Grunt plugins and it's configurations.

### BrowserSync
It's [super awesome](https://www.browsersync.io/). It has live reload, synced browsing, etc. Just use it.

Go to line **100** in your `Gruntfile.js` and replace it with the URL of your local dev environment. Virtual hosts works too.

    proxy: 'http://localhost/project-name/'

### FTP deploy

In case you still need to deploy stuff to the server via FTP/SFTP.

Go to line **170** in your `Gruntfile.js` and replace it with the remote host address.

    host: '127.0.0.1'

Go to line **174** in your `Gruntfile.js` and replace it with the remote folder path.

    dest: '/path/to/remote/'

And then update the `.ftppass` file with your server's login credentials.

### More configurations

The rest of the plugins should work out of the box. But different projects might have different requirements. Here's a list of all the plugins included in all starter kits and a link to the docs.

* [grunt-contrib-watch](#)
* [grunt-contrib-clean](#)
* [grunt-contrib-copy](#)
* [grunt-contrib-compass](#)
* [grunt-contrib-imagemin](#)
* [grunt-contrib-uglify](#)
* [grunt-contrib-concat](#)
* [grunt-autoprefixer](#)
* [grunt-browser-sync](https://www.npmjs.com/package/grunt-browser-sync)
* [grunt-ftp-deploy](https://www.npmjs.com/package/grunt-sftp-deploy)
* [grunt-wiredep](https://www.npmjs.com/package/grunt-wiredep)
* [load-grunt-tasks](#)

If your project requires additional automation, go check out Grunt's [plugin directory](#).

---

## Download frameworks & libraries

[Bower](#) is included in all the starter kits to simplify dependency management and installing additional libraries into your project. Simply run:

    bower install somelibraryname --save

Thanks to [grunt-wiredep](#), downloaded libraries will be automatically included in your template. But don't forget to add the `--save` flag to make sure it is included as a dependency.

**Notes**: for WordPress themes, scripts & libraries must be included manually in your `functions.php` file. See instructions on [how to include scripts in your WordPress theme](#).

    function theme_scripts() {
        // theme base styles and functions
        wp_enqueue_style( 'plugin-style', get_template_directory_uri() . '/bower_components/plugin/plugin.css', array(), '20120206', 'all' );
        wp_enqueue_script( 'plugin-js', get_template_directory_uri() . '/bower_components/plugin/plugin.min.js', array(), '20120206', true );
    }
