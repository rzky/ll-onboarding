# Lion & Lion Tech Onboarding

Greetings from Lion & Lion! This repo contains guides and documentations that explains how the Lion Tech division works.

Visit the [Wiki](https://bitbucket.org/lionandlion/ll-onboarding/wiki/) to browse through all available documents. Additionally, a buddy will also be assigned to you if you have any questions, and to help you get you past your first month :)

Welcome aboard!

---

**Note**: This repo is intended to be a living documentation of Lion & Lion tech standards and working practice. If you wish to contribute, please get in touch with the Head of Tech.  
